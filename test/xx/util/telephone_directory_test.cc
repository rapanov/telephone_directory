/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <cstdint>
#include <catch.hpp>
#include <xx/util/telephone_directory.hh>

namespace xx::util
{

TEST_CASE("xx::util::Telephone_directory")
{
  Telephone_directory dir;
  Telephone_directory::Optional<uint64_t> op_id;

  CHECK(!dir.add_prefix(123u, 1u));
  CHECK(!dir.add_number(12345u, 2u));
  CHECK(!dir.add_prefix(1234567u, 3u));
  CHECK(!dir.add_prefix(7903u, 1u));
  CHECK(!dir.add_number(7903u, 2u));
  CHECK(!dir.add_prefix(8u, 115u));
  CHECK(!dir.add_number(999u, 115u));
  CHECK(!dir.add_number(8326u, 200u));
  CHECK(!dir.add_number(0u, 800u));

  SECTION("Operator 1")
  {
    SECTION("Prefix 123")
    {
      op_id = dir.find_prefix(123u);
    }

    SECTION("Prefix 1234")
    {
      op_id = dir.find_prefix(1234u);
    }

    SECTION("Prefix 12345")
    {
      op_id = dir.find_prefix(12345u);
    }

    SECTION("Prefix 123456")
    {
      op_id = dir.find_prefix(123456u);
    }

    SECTION("Prefix 7903")
    {
      op_id = dir.find_prefix(7903u);
    }

    SECTION("Prefix 7903822")
    {
      op_id = dir.find_prefix(7903822u);
    }

    SECTION("Number 123")
    {
      op_id = dir.find_number(123u);
    }

    SECTION("Number 1234")
    {
      op_id = dir.find_number(1234u);
    }

    SECTION("Number 123456")
    {
      op_id = dir.find_number(123456u);
    }

    SECTION("Number 7903825")
    {
      op_id = dir.find_number(7903825u);
    }

    REQUIRE(op_id);
    CHECK(*op_id == 1u);
  }

  SECTION("Operator 2")
  {
    SECTION("Number 12345")
    {
      op_id = dir.find_number(12345u);
    }

    SECTION("Number 7903")
    {
      op_id = dir.find_number(7903u);
    }

    REQUIRE(op_id);
    CHECK(*op_id == 2u);
  }

  SECTION("Operator 3")
  {
    SECTION("Prefix 1234567")
    {
      op_id = dir.find_prefix(1234567u);
    }

    SECTION("Number 1234567")
    {
      op_id = dir.find_number(1234567u);
    }

    SECTION("Number 12345678")
    {
      op_id = dir.find_number(12345678u);
    }

    SECTION("Number 12345670")
    {
      op_id = dir.find_number(12345678u);
    }

    REQUIRE(op_id);
    CHECK(*op_id == 3u);
  }

  SECTION("Unknown operator")
  {
    CHECK(!dir.find_prefix(202u));
    CHECK(!dir.find_prefix(999u));
    CHECK(!dir.find_number(202u));
  }

  SECTION("Changing operator")
  {
    Telephone_directory::Optional<uint64_t> old_op_id;

    old_op_id = dir.add_number(12345u, 7u);
    op_id = dir.find_number(12345u);
    REQUIRE(op_id);
    REQUIRE(old_op_id);
    CHECK(*op_id == 7u);
    CHECK(*old_op_id == 2u);

    old_op_id = dir.add_prefix(12345u, 8u);
    op_id = dir.find_number(123456u);
    REQUIRE(op_id);
    CHECK(!old_op_id);
    CHECK(*op_id == 8u);
  }

  SECTION("Operator 115")
  {
    SECTION("Number 80")
    {
      op_id = dir.find_number(80u);
    }

    SECTION("Number 83265")
    {
      op_id = dir.find_number(83265u);
    }

    SECTION("Number 999")
    {
      op_id = dir.find_number(999u);
    }

    REQUIRE(op_id);
    CHECK(*op_id == 115u);
  }

  SECTION("Operator 200")
  {
    op_id = dir.find_number(8326u);
    REQUIRE(op_id);
    CHECK(*op_id == 200u);
  }

  SECTION("Operator 800")
  {
    op_id = dir.find_number(0u);
    REQUIRE(op_id);
    CHECK(*op_id == 800u);
  }
}

} // namespace xx::util
