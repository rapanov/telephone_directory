/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <cassert>
#include <memory>
#include <xx/util/telephone_directory.hh>

namespace xx::util
{

Telephone_directory::Optional<uint64_t>
Telephone_directory::add(uint64_t val, uint64_t operator_id, bool is_number)
{
  assert(trie_nodes_.size() > 0u);

  const Digits digits(val);
  const auto iter_end = digits.end();
  size_t node_idx = 0u;

  for (auto iter = digits.begin(); iter < iter_end; ++iter)
  {
    const Trie_node& node = trie_nodes_[node_idx];
    const uint8_t digit = *iter;
    assert(digit < node.next_node_indices.size());
    size_t next_node_idx = node.next_node_indices[digit];

    if (next_node_idx == Trie_node::invalid_index)
    {
      // Create the new node
      next_node_idx = trie_nodes_.size();
      trie_nodes_.emplace_back();
      trie_nodes_[node_idx].next_node_indices[digit] = next_node_idx;
    }

    node_idx = next_node_idx;
  }

  Trie_node& node = trie_nodes_[node_idx];
  Optional<uint64_t>& operator_id_ref = node.operator_id(is_number);
  const Optional<uint64_t> old_operator_id = operator_id_ref;
  operator_id_ref = operator_id;

  return old_operator_id;
}

Telephone_directory::Optional<uint64_t>
Telephone_directory::find(uint64_t val, bool is_number) const noexcept
{
  assert(trie_nodes_.size() > 0u);

  const Digits digits(val);
  const auto iter_end = digits.end();
  Optional<uint64_t> result;
  size_t node_idx = 0u;

  for (auto iter = digits.begin(); ; ++iter)
  {
    const bool is_last = iter == iter_end;
    const Trie_node& node = trie_nodes_[node_idx];

    if (is_number && is_last && node.number_operator_id)
    {
      result = *node.number_operator_id;
    }
    else if (node.prefix_operator_id)
    {
      result = *node.prefix_operator_id;
    }

    if (is_last)
    {
      break;
    }

    const uint8_t digit = *iter;
    assert(digit < node.next_node_indices.size());
    const size_t next_node_idx = node.next_node_indices[digit];

    if (next_node_idx == Trie_node::invalid_index)
    {
      break;
    }

    node_idx = next_node_idx;
  }

  return result;
}

} // namespace xx::util
