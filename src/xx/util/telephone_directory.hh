/**
 * Copyright (C) 2020, Roman Panov (roman.a.panov@gmail.com).
 */

#include <algorithm>
#include <array>
#include <cstdint>
#include <limits>
#include <optional>
#include <vector>

namespace xx::util
{

// This is an implementation of Telephone Directory based on an uncompressed
// trie. Uncompressed trie is simple and fast, but not as memory efficient
// as compressed trie. Compressed trie is also fast, but is a headache to code
// and debug. Therefore I've stuck to the uncompressed one here.
//
// Complexity:
// Find operations run in O(1) (constant) time.
// Add operations run in amortized constant time. Amortized because they employ
// emplace_back() to vector, which may entail reallocation. But this in fact
// happens on rare occasion, because when vector exhausts its allocated capacity
// the capacity is doubled.

struct Telephone_directory
{
  template <class T>
  using Optional = ::std::optional<T>;

  Telephone_directory()
    : trie_nodes_(1u) // One root node is always present.
  {}

  Optional<uint64_t> add_prefix(uint64_t prefix, uint64_t operator_id)
  {
    return add(prefix, operator_id, false);
  }

  Optional<uint64_t> add_number(uint64_t number, uint64_t operator_id)
  {
    return add(number, operator_id, true);
  }

  Optional<uint64_t> find_prefix(uint64_t prefix) const noexcept
  {
    return find(prefix, false);
  }

  Optional<uint64_t> find_number(uint64_t number) const noexcept
  {
    return find(number, true);
  }

private:
  template <class T, size_t n>
  using Array = ::std::array<T, n>;

  template <class T>
  using Vector = ::std::vector<T>;

  class Digits
  {
    static constexpr size_t max_digits_ = 20u;
    using Digits_array = Array<uint8_t, max_digits_>;

  public:
    using Iterator = Digits_array::iterator;
    using Const_iterator = Digits_array::const_iterator;

    explicit Digits(uint64_t val) noexcept
      : begin_(digits_.end())
    {
      do
      {
        *--begin_ = static_cast<uint8_t>(val % ten_);
        val /= ten_;
      }
      while (val);
    }

    Const_iterator begin() const noexcept
    {
      return begin_;
    }

    Const_iterator end() const noexcept
    {
      return digits_.end();
    }

  private:
    static constexpr uint64_t ten_ = uint64_t(10u);

    Digits_array digits_;
    Iterator     begin_;
  };

  struct Trie_node
  {
    Trie_node() noexcept
    {
      ::std::fill(
        next_node_indices.begin(), next_node_indices.end(), invalid_index);
    }

    Optional<uint64_t>& operator_id(bool is_number) noexcept
    {
      if (is_number)
      {
        return number_operator_id;
      }

      return prefix_operator_id;
    }

    static constexpr size_t invalid_index =
      ::std::numeric_limits<size_t>::max();

    Array<size_t, 10u> next_node_indices;
    Optional<uint64_t> prefix_operator_id;
    Optional<uint64_t> number_operator_id;
  };

  Optional<uint64_t> add(uint64_t val, uint64_t operator_id, bool is_number);
  Optional<uint64_t> find(uint64_t val, bool is_number) const noexcept;

  Vector<Trie_node> trie_nodes_;
};

} // namespace xx::util
