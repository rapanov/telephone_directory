# Copyright 2020, Roman Panov (roman.a.panov@gmail.com).

add_library(
  xx STATIC
  xx/util/telephone_directory.cc)

target_include_directories(
  xx PUBLIC
  "${CMAKE_CURRENT_SOURCE_DIR}")